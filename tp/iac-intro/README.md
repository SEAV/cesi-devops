# Introduction Infrastructure as Code

Le but de ce TP est de vous faire appréhender des outils  mettant en oeuvre de l'infrastructe-as-code.

> Le TP doit être réalisé en groupe.

<!-- vim-markdown-toc GitLab -->

* [0. `git init`](#0-git-init)
* [I. Vagrant](#i-vagrant)
    * [1. Mise en jambe](#1-mise-en-jambe)
    * [2. Accélération du déploiement](#2-accélération-du-déploiement)
    * [3. Configuration de la VM](#3-configuration-de-la-vm)
    * [4. Ajout d'un node](#4-ajout-dun-node)
* [II. Ansible](#ii-ansible)
    * [1. Un premier playbook](#1-un-premier-playbook)
    * [2. Création de playbooks](#2-création-de-playbooks)
* [III. Development techniques](#iii-development-techniques)
    * [Mise en place de tests](#mise-en-place-de-tests)

<!-- vim-markdown-toc -->

# 0. `git init`

Dans cette section, vous allez mettre en place un élément important de votre environnement de travail : un dépôt Git.

Première étape : créer votre dépôt git. Il vous permettra de :
* travailler collaborativement
* travailler en parallèle
* conserver un historique de votre travail
* s'intégrer avec d'éventuelles évolutions

Pour le TP, et pour des raisons que nous verrons un peu plus loin, vous utiliserez l'instance publique de [Gitlab](https://gitlab.com) comme hébergeur git.

Sur Gitlab :
* créez-vous un compte par personne
* créer un "Groupe" sur Gitlab (aussi parfois appelé "organisation")
* depuis le groupe créé, créez un nouveau projet
* dans le projet créé, attribuez les droits à tous les membres pour pouvoir l'utiliser

**Assurez-vous que tout le monde peut `git pull` et `git push` sur le dépôt.**

> Vous hébergerez dans ce dépôt Git tout votre travail réalisé dans la suite du TP. Vous pourrez ainsi pratiquer tout en conservant une trace de votre travail.

---

Quelques règles d'utilisation de Git :
* faites des commits régulièrement
* utilisez des messages de commit explicites
* éviter d'utiliser la branche 'master' pour développer
  * le branche master ne doit être modifié qu'avec des merges d'autres branches
* utiliser le fichier `.gitignore`

# I. Vagrant

Ici, on va mettre en place un environnement virtualisé avec Vagrant. On va utiliser l'OS CentOS7 pour réaliser les tests.

## 1. Mise en jambe

Pour réaliser ce travail, je vous conseille de tous créer une branche sur le dépôt Git : une par personne, afin de travailler chacune dans votre coin.

```bash
# Créez vous un répertoire de travail
$ mkdir vagrant
$ cd vagrant

# Initialisez un Vagrantfile
$ vagrant init centos/7
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

La commande `vagrant init` permet de générer un fichier `Vagrantfile`. Ce fichier contient tout le nécessaire pour allumer une VM CentOS7.

> Je vous invite à lire le `Vagrantfile` pour voir les choses que l'on peut réaliser.

Une fois le `Vagrantfile` généré, épurez-le en enlevant les commentaires, vous devriez vous retrouver avec une version minimale qui ressemble à ça :

```ruby
Vagrant.configure("2")do|config|
  config.vm.box="centos/7"
end
```
Pour lancer la VM :
```bash
$ vagrant up
[...]

# Vous pouvez voir l'état des VMs liées au Vagrantfile
$ vagrant status

# Une fois le déploiement terminé vous pouvez SSH dans la VM grâce à Vagrant
$ vagrant ssh
```

## 2. Accélération du déploiement

Au lancement, la machine CentOS7 fait plusieurs installations. Il serait intéressant de pouvoir juste lancer la machine avec ces installations déjà réalisées.

On va utiliser un mécanisme de Vagrant : le packaging. On va packager une box toute faite avec toutes nos installs à l'intérieur. On pourra ré-utiliser cette box par la suite.

Définissez le `Vagrantfile` suivant :
```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"

  config.vm.synced_folder '.', '/vagrant', disabled: true

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end

  # Setup swarm cluster and some other shit, see script
  config.vm.provision "shell", path: "./setup.sh"

end
```

Et, dans le même dossier que le `Vagrantfile`, créez le fichier `setup.sh` contenant :
```bash
#!/bin/bash

# Ajout des dépôts EPEL
yum install -y epel-release

# Mise à jour du système
yum update -y

# Démarrage des vbox invités
systemctl enable vboxadd
systemctl enable vboxadd-service

# Clean caches and artifacts
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
```

Comme défini dans le `Vagrantfile`, le script `setup.sh` sera exécuté au lancement de la VM.

Une fois ces deux fichiers en place, vous pouvez :
```bash
$ vagrant up
```

Lorsque la VM sera allumée (avec les installations réalisées), on va pouvoir repackager la box :
```bash
$ vagrant package --output centos7-custom.box
$ vagrant box add centos7-custom centos7-custom.box
```

Enfin, il ne reste qu'à tester, en créant un nouveau Vagrantfile :
```ruby
Vagrant.configure("2")do|config|
  # Notre box custom
  config.vm.box="centos7-custom"
end
```

Go `vagrant up` !

> Par défaut, Vagrant synchronise tout le répertoire contenant le Vagrantfile dans la VM au démarrage de celle-ci. Créez donc un répertoire dédié afin de ne pas synchroniser de choses superflues.

## 3. Configuration de la VM

Modifier le Vagrantfile (et/ou le `setup.sh`) pour ajouter à la VM :
* une IP statique choisie
* un disque supplémentaire (3Go)
  * le disque supplémentaire devra se trouver dans le même répertoire que le Vagrantfile
* le paquet `vim`
* le paquet `python3`

Poussez votre travail sur votre banche Git. **Utilisez un fichier `.gitignore` pour empêcher l'ajout des fichiers non pertinents au dépôt Git (box Vagrant, disque de la VM, etc.)**.

## 4. Ajout d'un node

Modifier le Vagrantfile pour ajouter une deuxième VM. Un seul `vagrant up` et deux VMs s'allument.

Les deux VMs doivent, une fois allumées, pouvoir se `ping`.

Vous pouvez vous inspirer de [la doc officielle pour créer deux machines en un seul Vagrantfile](https://www.vagrantup.com/docs/multi-machine/). Par exemple :
```ruby
Vagrant.configure("2") do |config|
  
  # Configuration commune à toutes les machines
  config.vm.box = "centos7-custom"
  config.vm.box_check_update = false # désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
  end

  # Config une première VM "node1"
  config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "192.168.56.11"
  end

  # Config une première VM "node2"
  config.vm.define "node2" do |node2|
    node2.vm.network "private_network", ip: "192.168.56.12"
  end
end
```

---

Une fois ce `Vagrantfile` final réalisé sur votre banche Git, **effectuez un *merge* afin de pousser votre travail vers la branche *master***.

# II. Ansible

Ici, vous allez vous faire la main sur vos premiers *playbooks* Ansible.

Un peu de vocabulaire lié à Ansible :
* *inventory*
  * l'inventaire est la liste des hôtes
  * les hôtes peuvent être groupés dans un groupe qui porte un nom
* *task*
  * une tâche est une opération de configuration
  * par exemple : ajout d'un fichier, création d'un utilisateur, démarrage d'un service, etc.
* *role*
  * un rôle est un ensemble de tâches qui a un but précis
  * par exemple :
    * un role "apache" : il installe Apache, le configure, et le lance
    * un role "users" : il déploie des utilisateurs sur les machines
* *playbook*
  * un *playbook* est le lien entre l'inventaire et les rôles
  * un *playbook* décrit à quel noeud on attribue quel rôle

## 1. Un premier playbook

Juste pour jouer, mettre en place Ansible et appréhender l'outil, on va rédiger un premier playbook dans un seul fichier.

Afin de pouvoir utilisez facilement Ansible, il est recommandé d'utiliser une machine virtuelle si vous êtes sous Windows.

---

On va ajouter simplement un serveur Web à notre machine déployée par Vagrant.

* Mettez en place une connexion SSH de la machine qui a Ansible (le "control node") vers la machine qui recevra la configuration
* Créez un playbook minimaliste `nginx.yml` :
```yaml
---
- name: Install nginx
  hosts: cesi
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install nginx
    yum:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
```

Et créez un inventaire `hosts.ini` :
```
---
[cesi]
<VM_IP>
```

Enfin, créez un fichier `index.html.j2` :
```
Hello from {{ ansible_default_ipv4.address }}
```

* Lancez le playbook !
```
$ ansible-playbook -i hosts.ini nginx.yml
```

**Vérifier le bon fonctionnement du site web !**

## 2. Création de playbooks

> Travaillez chacun sur vos branches git.

Créez un playbook qui :
* installe MariaDB
* lance MariaDB
* ajoute une base de données

Puis un dernier playbook qui :
* ajoute un utilisateur
* lui dépose une clé SSH

**Vérifier que tout fonctionne.**

Pousser une version fonctionnelle des *playbooks* sur a branche *master* (en faisant un *merge*)

# III. Development techniques

## Mise en place de tests

Dans cette dernière étape, on va mettre an place des tests très simplistes sur ce qu'on vient de produire afin d'appréhender le déroulement de tests automatisés.

Grâce à Gitlab, il est possible de mettre en palce des tests automatisés sur le contenu du dépôt. 

A titre d'exemple, nous allons tester :
* la bonne syntaxe du fichier Vagrantfile
* la bonne syntaxe des fichiers `.yml`

La configuration des tests avec Gitlab se fait simplement *via* l'ajout d'un fichier `.gitlab-ci.yml` à la racine du dépôt. Un exemple pourrait ressembler à :
```yml
stages:
  - first-test

first-test:
  stage: first_test
  image: debian
  script:
    - echo 'toto'
```

Une fois ce fichier ajouté au dépôt, le test devrait être exécuté automatiquement à chaque push sur la branche contenant le fichier `.gitlab-ci.yml`.

Les tests sont visibles dans l'onglet `CI/CD` du panneau latéral. **Vérifier que le test s'exécute correctement.**

> **Les tests sont exécutés dans des conteneurs Docker** : c'est pour ça que l'on précise une `image`.

---

A réaliser :
* trouver une commande qui permet de tester la bonne syntaxe d'un fichier yml
* trouver une commande qui permet de tester la bonne syntaxe d'un fichier Vagrant
* ajouter dans le `.gitlab-ci.yml` l'exécution de ces commandes afin de vérifier les fichiers de votre dépôt Git
