# Cours DevOps CESI 2020

Vous pourrez trouver ici les ressources liées au cours :
* [Mémo](./memo.md)
* [Les diapos vus en cours](./slides)
* [Des ressources externes](./external_resources/)
  * [Cheatsheet PDF git](./external_resources/froggit_git_antiseche.pdf)
  * [Cheatsheet PDF vagrant](./external_resources/vagrant-cheat-sheet.pdf)
