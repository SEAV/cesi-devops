# Mémos

Ici vous pourrez trouver des mémos concernant des commandes/outils que l'on va souvent utiliser dans les TPs.

<!-- vim-markdown-toc GitLab -->

* [Mémo Git](#mémo-git)
    * [Commandes récurrentes](#commandes-récurrentes)
    * [Un workflow typique git](#un-workflow-typique-git)
    * [Quelques autres commandes](#quelques-autres-commandes)
    * [Gestion de branche](#gestion-de-branche)
* [Mémo Vagrant](#mémo-vagrant)

<!-- vim-markdown-toc -->

## Mémo Git

Il existe plusieurs bonnes docs à ce sujet sur internet. **Vous pourrez trouver [un mémo PDF dans ce dépôt](./external_resources/froggit_git_antiseche.pdf).**

### Commandes récurrentes

Récupérer un dépôt existant :
```bash
# Vous pouvez récupérer un dépôt soit en SSH soit en HTTP. 
# Je vous conseille SSH pour nos cours, afin d'avoir un meilleur niveau de sécurité, et de ne pas saisir votre password à chaque opération.
$ git clone <REPO_ADDRESS>

# Par exemple
$ git clone git@gitlab.com:it4lik/cesi-devops.git     # clone en SSH
$ git clone https://gitlab.com/it4lik/cesi-devops.git # clone en HTTP
```

Ajout ou supprimer un fichier de Git :
```bash
# On ajoute un fichier à Git après avoir ajouter un fichier au dépôt, ou modifié un fichier existant du dépôt
$ git add <FILE>

# Par exemple
$ git add README.md

# On peut aussi supprimer un fichier de git
$ git rm <FILE>
```

Pour valider un changement (ajout, suppression, modification), il faut faire un `commit` :
```bash
# Il est obligatoire de préciser un "message de commit" : une phrase qui décrit le contenu des modifications
$ git commit -m "add README.md"
```

Pour pousser un changement sur le dépôt central :
```
$ git push
```

### Un workflow typique git

Un travail habituel avec git peut ressembler à la suite de commandes :
```bash
$ git clone REPO
$ cd REPO

$ vim file1
$ git add file1
$ vim file2
$ git add file2
$ git commit -m "add file1 and file2"

$ mkdir dir
$ vim dir/file1
$ git add dir
$ git commit -m "add file1 into dir"

$ git push
```

### Quelques autres commandes

```bash
# Afficher les logs du dépôt
$ git log
$ git log --oneline

# Affiche l'état de votre dépôt
$ git status
```

### Gestion de branche

```bash
# Liste des branches connues
$ git branch    # uniquement les branches locales
$ git branch -a

# Changer de branche, en la créant si elle n'existe pas
$ git checkout -b <NEW_BRANCH>
# Par exemple
$ git checkout -b test

# Merge d'une branche
## On se place sur la branche de destination
$ git checkout <BRANCH_DST>

## On merge la branche source
$ git merge <BRANCH_SRC>

## Par exemple, pour intégrer le contenu de la branche test à la branche master
$ git checkout master
$ git merge test
```

## Mémo Vagrant

Ici aussi, plusieurs bonnes ressources existent déjà sur le web, [**vous pourrez en trouver une dans le dépôt en PDF](./external_resources/vagrant-cheat-sheet.pdf).**

---

Initialiser un nouveau Vagrantfile :
```bash
$ vagrant init <BOX>

# Par exemple
$ vagrant init centos/7
```

Allumer les VMs définies dans un Vagrantfile
```bash
$ ls
Vagrantfile

$ vagrant up
```

Voir l'état des VMs 
```bash
$ vagrant status
```

Se connecter à une VM
```bash
# Une seule VM
$ vagrant ssh

# Plusieurs VMs
$ vagrant status
$ vagrant ssh <MACHINE_NAME>
```
